package com.mycompany.freemarkerform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FreemarkerFormApplication {

	public static void main(String[] args) {
		SpringApplication.run(FreemarkerFormApplication.class, args);
	}

}
